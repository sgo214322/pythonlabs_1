from datetime import datetime

def getDate():
    currentDate = datetime.now()
    day = currentDate.day
    month = currentDate.month
    if day < 10:
        day = f"0{day}"
    if month < 10:
        month = f"0{month}"
    currentDate = f"{day}.{month}.{currentDate.year}"

    return currentDate

def createSQLScript(readyDataBaseList, scriptPath):
    with open(scriptPath, "w") as f:
        for dataBase in readyDataBaseList:
            f.write(f'USE [{dataBase}]\n')
            f.write("GO\n")
            f.write(f"DBCC SHRINKDATABASE(N'{dataBase}')\n")
            f.write("GO\n")

def createLogs(readyDataBaseList, failDataBaseDict, logsPath):
    with open(logsPath, "a") as f:
        today = datetime.today().strftime("%d.%m.%Y %H:%M:%S")
        today = f"{today}>"
        for dataBase in readyDataBaseList:
            f.write(f"{today} {dataBase}: OK.\n")
        for key in failDataBaseDict:
            f.write(f"{today} {key}: {failDataBaseDict[key]}\n")
        if len(readyDataBaseList) == 0 and len(list(failDataBaseDict.keys())) == 0:
            f.write(f"{today} Базы данных не сжимались\n")
        f.write("==========================================\n")

def getInformation(msLogsPath):
    date = getDate()
    # date = "11.03.2022"

    with open("ModelStudio.log", "r") as f:
        mass = f.readlines()

    str = list()
    for line in mass:
        str.append(line)

    str = "".join(str)
    mass = str.split("Model studio CS log started.")
    sublines = mass[-1].split("\n")

    operations = list()
    for line in sublines:
        if date in line:
            operations.append(line)

    readyDataBaseList = list()
    failDataBaseDict = dict()

    resMass = list()
    for i in range(len(operations)):
        operation = operations[i].split(">")[1]
        if "Завершено исполнение команды CleanObjects" in operation:
            if "OK" in operation:
                resMass.append("OK")
            else:
                resMass.append(operation.split("Завершено исполнение команды CleanObjects")[1][2::])

    countRes = 0
    for i in range(len(operations)):
        operation = operations[i].split(">")[1]
        if "Открыто соединение с базой данных" in operation:
            dataBase = operation.split(" на SQLSERVER")[0].split(" Открыто соединение с базой данных ")[1]
            if resMass[countRes] == "OK":
                readyDataBaseList.append(dataBase)
                countRes += 1
        else:
            if "баз" in operation and "ошибка" in operation.lower() and "завершено исполнение команды" not in operation.lower():
                temp = operation.split()
                indBase = [j for j in range(len(temp)) if "баз" in temp[j]][0]
                dataBase = " ".join(temp[indBase + 1::]).split(" в команде CleanObjects")[0]
                failDataBaseDict[dataBase] = resMass[countRes]
                countRes += 1

    return (readyDataBaseList, failDataBaseDict)

if __name__ == "__main__":
    # Сбор информации по операциям Model Studio CS log на текущую дату
    readyDataBaseList, failDataBaseDict = getInformation("ModelStudio.log")
    # Создание SQL скрипта
    createSQLScript(readyDataBaseList, "Shrink.sql")
    # Добавление в лог
    createLogs(readyDataBaseList, failDataBaseDict, "ShrinkLogs.log")