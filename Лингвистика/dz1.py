import xlsxwriter
import re

opt = "Я не люблю смотреть телевизионные передачи. Но были программы, которые я смотрел всегда: танцы на льду. Потом я устал от них и смотреть перестал — перестал систематически, смотрю только эпизодически. Больше всего мне нравится, когда те, кого считают слабыми или кто еще не вошел в обоймы “признанных”, выступают удачно. Удача начинающих или удача неудачливых приносит гораздо более удовлетворения, чем удача удачников. Но дело не в этом. Больше всего меня восхищает, как “конькобежец” (так в старину называли спортсменов на льду) выправляет свои ошибки во время танца. Упал и встает, быстро вступая снова в танец, и ведет этот танец так, точно падения и не было. Это искусство, огромное искусство. Но ведь в жизни ошибок бывает гораздо больше, чем на ледяном поле. И надо уметь выходить из ошибок: исправлять их немедленно и... красиво. Да, именно красиво. Когда человек упорствует в своей ошибке или чересчур переживает, думает, что жизнь кончилась, “все погибло”, — это досадно и для него и для окружающих. Окружающие испытывают неловкость не от самой ошибки, а от того, какое неумение проявляет ошибшийся в ее исправлении. Признаться в своей ошибке перед самим собой (не обязательно делать это публично: тогда это либо стыдно, либо рисовка) не всегда легко, нужен опыт. Нужен опыт, чтобы после совершенной ошибки как можно скорее и как можно легче включиться в работу, продолжить ее. И окружающим не надо понуждать человека к признанию ошибки, надо побуждать к ее исправлению; реагируя так, как реагируют зрители на соревнованиях, иногда даже награждая упавшего и легко исправившего свою ошибку радостными аплодисментами при первом же удобном случае."
text = re.sub(r'[^\w\s]','', opt)
textMass = text.split()
textMass = [word.lower() for word in textMass]
print(len(textMass))

text = textMass.copy()

rang = dict()
for i in range(len(text)):
    rang[text[i]] = 0
for i in range(len(text)):
    rang[text[i]] += 1

print(rang)

sorted_values = sorted(rang.values())
sorted_dict = {}

for i in sorted_values:
    for k in rang.keys():
        if rang[k] == i:
            sorted_dict[k] = rang[k]

print(sorted_dict)

wordRang = list(sorted_dict.keys())
finalWords = list()
for i in range(len(wordRang)-1, -1, -1):
    finalWords.append(wordRang[i])
print(finalWords)

workbook = xlsxwriter.Workbook(f'dz1.xlsx')
worksheet = workbook.add_worksheet()

bold = workbook.add_format({'bold': True})
worksheet.write("B1", "Ранг", bold)
worksheet.write("C1", "Словоформа", bold)
worksheet.write("D1", "i", bold)
worksheet.write("E1", "i норм", bold)
worksheet.write("F1", "0.1r-1", bold)

for i in range(len(finalWords)):
    worksheet.write(f'A{i + 2}', i+1)
    if sorted_dict[finalWords[i]] == 4:
        rangInt = 4
        worksheet.write(f'B{i+2}', 4)
    elif sorted_dict[finalWords[i]] == 3:
        rangInt = 5
        worksheet.write(f'B{i + 2}', 5)
    elif sorted_dict[finalWords[i]] == 2:
        rangInt = 6
        worksheet.write(f'B{i + 2}', 6)
    elif sorted_dict[finalWords[i]] == 1:
        rangInt = 7
        worksheet.write(f'B{i + 2}', 7)
    elif sorted_dict[finalWords[i]] == 8:
        rangInt = 3
        worksheet.write(f'B{i + 2}', 3)
    elif sorted_dict[finalWords[i]] == 9:
        rangInt = 2
        worksheet.write(f'B{i + 2}', 2)
    elif sorted_dict[finalWords[i]] == 11:
        rangInt = 1
        worksheet.write(f'B{i + 2}', 1)
    worksheet.write(f'C{i+2}', finalWords[i])
    worksheet.write(f'D{i + 2}', sorted_dict[finalWords[i]])
    worksheet.write(f'E{i + 2}', round(sorted_dict[finalWords[i]] / 174, 3))
    worksheet.write(f'F{i + 2}', round(0.1 * (1 / rangInt), 3))

workbook.close()
